package org.bts.android.media_player_1;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity  {

    private ArrayList<Music> arrayList;
    private CustomMusicAdapter adapter;
    private ListView songList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        songList =(ListView) findViewById(R.id.songList);
        arrayList = new ArrayList<>();
        arrayList.add(new Music("Brazilian Samba","bensound",R.raw.bensoundbrazilsamba));
        arrayList.add(new Music("Country Boy","bensound",R.raw.bensoundcountryboy));
        arrayList.add(new Music("India","bensound",R.raw.bensoundindia));
        arrayList.add(new Music("Little Planet","bensound",R.raw.bensoundlittleplanet));
        arrayList.add(new Music("Psychedelic","bensound",R.raw.bensoundpsychedelic));
        arrayList.add(new Music("Relaxing","bensound",R.raw.bensoundrelaxing));
        arrayList.add(new Music("Thee levator Bossanova","bensound",R.raw.bensoundtheelevatorbossanova));

        adapter = new CustomMusicAdapter(this, R.layout.custom_music_item, arrayList);
        songList.setAdapter(adapter);

    }


}
